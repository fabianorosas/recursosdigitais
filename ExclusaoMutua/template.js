function sketchProc(processing) {
    var img_cache_direto;
    
    processing.setup = function() {
	img_cache_direto = processing.loadImage("Fundo1.png");
	processing.size(522, 542);
    };
    
    processing.draw = function() {
	processing.image(img_cache_direto, 0, 0);
    };
}

var canvas = document.getElementById("canvas");
var processingInstance = new Processing(canvas, sketchProc);
