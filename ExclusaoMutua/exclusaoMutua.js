var canvas = document.getElementById("canvas");
var processingInstance = new Processing(canvas, sketchProc);

function sketchProc(processing) {
    var img_exclusaoMutua;
    var ax1;
    var ax2;
    var lock = 0;
    var mostrarSeta1 = -1;
    var mostrarSeta2 = -1;
    var n = 28;
    var movimento = false;
    var core;
    var xLock, yLock, xAx, yAx;
    
    
    processing.setup = function() {
	img_exclusaoMutua  = processing.loadImage("./data/Fundo1.png");
	processing.size(721, 644);
    };
    
    processing.draw = function() {
		processing.image(img_exclusaoMutua, 0, 0);	

		//Seta 1
		if(mostrarSeta1 >= 0){
			processing.noStroke();
			processing.fill(255, 3, 19);
			processing.triangle(205, 280+(mostrarSeta1 * n), 205, 320+(mostrarSeta1 * n),225,300+(mostrarSeta1 * n));
			processing.rect(175,292+(mostrarSeta1*n),30,16);
		}
		
		//Seta 2
		if(mostrarSeta2 >= 0){
			processing.noStroke();
			processing.fill(255, 3, 19);
			processing.triangle(517, 280+(mostrarSeta2*n), 517, 320+(mostrarSeta2*n),497,300+(mostrarSeta2*n));
			processing.rect(517,292+(mostrarSeta2*n),30,16);
		}
		

		//Texto
		if(movimento ==  false){		
			processing.textSize(60);
			processing.fill(0);
			processing.text(lock,322,107);
			processing.text(ax1,120,208);
			processing.text(ax2,615,208);
		}
		
		//Movimento
		if(movimento){
			processing.textSize(60);
			processing.fill(0);
			if(core == 1){
				processing.text(ax2,615,208);
				processing.text(ax1,xLock,yLock);
				processing.text(lock,xAx,yAx);
				if(xLock > 120){
					xLock--;
				}
				if(yLock < 208){
					yLock++;
				}
				if(xAx < 322){
					xAx++;
				}
				if(yAx > 107){
					yAx--;
				}
				if((xLock == 120) && (yLock == 208) && (xAx == 322) && (yAx == 107)){
					mostrarSeta1++;
					movimento = false;
				}								
			}			
			if(core == 2){
				processing.text(ax1,120,208);
				processing.text(ax2,xLock,yLock);
				processing.text(lock,xAx,yAx);
				if(xLock < 615){
					xLock++;
				}
				if(yLock < 208){
					yLock++;
				}
				if(xAx > 322){
					xAx--;
				}
				if(yAx > 107){
					yAx--;
				}
				if((xLock == 615) && (yLock == 208) && (xAx == 322) && (yAx == 107)){
					mostrarSeta2++;
					movimento = false;
				}												
			}
		}		
    };

    document.getElementById("btncore1").onclick = function (){
		if(movimento == false){
			switch(mostrarSeta1){
			case -1:
					mostrarSeta1++;
					break;
			case 0:
					mostrarSeta1 = mostrarSeta1 + 2;
					ax1 = 1;
					break;							
			case 2:
					movimento = true;			
					core = 1;					
					xLock = 322;
					yLock = 107;
					xAx = 120;
					yAx = 208;
					aux = ax1;
					ax1 = lock;
					lock = aux;
					break;
			case 3:
					mostrarSeta1++;
					break;
			case 4:
					if(ax1 == 1){
						mostrarSeta1 = 2;
					}
					else{
						mostrarSeta1 = mostrarSeta1 + 2;
					}
					break;
			case 6:
					mostrarSeta1 = mostrarSeta1 + 2;
					break;
			case 8:
					lock = 0;
					mostrarSeta1 = -2;
					break;
			}
		}
		
    };

    document.getElementById("btncore2").onclick = function (){
		if(movimento == false){
			switch(mostrarSeta2){
			case -1:
					mostrarSeta2++;
					break;
			case 0:
					mostrarSeta2 = mostrarSeta2 + 2;
					ax2 = 1;
					break;
			case 2:
					movimento = true;					
					core = 2;
					xLock = 322;
					yLock = 107;
					xAx = 615;
					yAx = 208;
					aux = ax2;
					ax2 = lock;
					lock = aux;
					break;
			case 3:
					mostrarSeta2++;
					break;
			case 4:
					if(ax2 == 1){
						mostrarSeta2 = 2;
					}
					else{
						mostrarSeta2 = mostrarSeta2 + 2;
					}
					break;
			case 6:
					mostrarSeta2 = mostrarSeta2 + 2;
					break;
			case 8:
					lock = 0;
					mostrarSeta2 = -2;
					break;
			}
		}
    };
}


