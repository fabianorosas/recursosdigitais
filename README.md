Este ramo dedica-se aos projetos que ainda estão em desenvolvimento.


Trabalhos terminados encontram-se no ramo "master" e versões completas(contendo os recursos usados como fonte) estão nos arquivos .zip da seção *Downloads* do site.

---

O projeto está sob o controle de versão do git (dúvidas?: http://git-scm.com/book/), o bitbucket é apenas a interface que usaremos para gerenciar questões de alto nível.
Os comandos básicos do git(em linha de comando) são:

COMANDOS FUNDAMENTAIS
-----------------------

```
git clone <url>
```
clona o repositorio para o seu computador, criando uma pasta chamada recursosdigitais, onde estarão todos os arquivos contidos no repositorio. Esta pasta será a sua cópia local e qualquer modificação feita, afetará apenas os arquivos locais, a não ser que uma operação de push seja efetuada. O comando clone é geralmente efetuado apenas uma vez.


```
git add <algumacoisa>
```
Adiciona o arquivo ou diretório ao controle de versão. Este comando deve ser rodado sempre que um arquivo for modificado. Um exemplo de uso comum é: "git add ." que adiciona toda a árvore de diretórios atual ao sistema. O comando "git status" citado acima, mostra quais arquivos foram modificados e precisam ser adicionados. Outro exemplo seria "git add ./teste.txt", que adiciona apenas o arquivo teste.txt.


```
git commit -m "Mensagem"
```
Efetua a transação e adiciona uma mensagem. É o que dirá que estas modificações estão prontas para serem enviadas ao repositório. Isso também iinforma às outras pessoas quais foram as modificações que você efetuou naquele momento.


```
git push
```
Envia todas as suas alterações para o repositório. Geralmente utilizado ao encerrar a jornada de trabalho ou quando se deseja que as outras pessoas tenham acesso ao seu trabalho. Não esqueça deste comando, pode ser que (sempre) as outras pessoas necessitem do seu código atualizado.


```
git fetch
```
Busca as alterações no repositório, mas não as aplica.


```
git pull
```
Traz do repositório as modificações de outras pessoas. Geralmente utilizado ao iniciar a jornada de trabalho, em conjunto com git fetch e git status.

COMANDOS DE ANALISE
---------------------

```
git status
```
Mostra um pequeno relatório sobre os arquivos que estão no diretório sob o controle de versão. Use este comando com frequência.


```
git log
```
Exibe um log dos commits realizados. Útil quando você realizou vários commits e esqueceu o que fez.


```
git branch -v
```
Mostra o último commit de cada ramo (vide abaixo)

COMANDOS DE RAMIFICAÇÃO (http://git-scm.com/book/en/Git-Branching-Basic-Branching-and-Merging)
------------------------

```
git branch <nome>
```
Cria uma ramificação no repositório. Evita que as suas modificações influenciem o ramo principal do repositório. Útil quando for realizar modificações críticas que podem causar grande estrago. Após trabalhar no seu ramo, é possível efetuar um pedido ('pull request' no site) para mesclar o seu ramo com o principal.


```
git checkout <nomedobranch>
```
Muda para o branch especificado. A partir deste ponto, os comandos executados serão aplicados neste ramo.


```
git branch -d <nomedobranch>
```
Deleta o ramo de nome especificado.

EXEMPLO DE WORKFLOW BÁSICO
----------------------------
Geralmente, o trabalho utilizando o git é feito da seguinte forma:

*----------apenas uma vez-------------------------------------------------------------------------*

-Cria-se a pasta local com o comando clone (o comando pode ser copiado do site)

*----------ao iniciar a sessão--------------------------------------------------------------------*

1-Busca-se as mudanças que estão no repositório, mas não estão em sua cópia local (desnecessário na primeira vez) com os comandos fetch e  pull

*----------algumas vezes durante a sessao---------------------------------------------------------*

2-Modifica-se os arquivos à vontade

3-Marca-se os arquivos para serem enviados ao repo, com o comando add

4-Grava-se as modificações feitas, deixando um breve texto explicativo, com o comando commit

*----------ao final da sessao, ou conforme as outras pessoas forem precisando---------------------*

5-Envia-se as mudanças para o repositório com o comando push


