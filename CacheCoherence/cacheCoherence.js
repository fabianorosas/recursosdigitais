var canvas = document.getElementById("canvas");
var processingInstance = new Processing(canvas, sketchProc);

function sketchProc(p){
    var img_cache_coherence;
    var nucleos;
    var ram;
    var desenhaSeta;
    var neverMoved = true;
    var linha;

    p.setup = function (){
	img_cache_coherence = p.loadImage("fundo.png");
	p.size(1020, 517);
	p.frameRate(80);
	p.textSize(26);

	//prototipo dos nucleos
	nucleo = {
	    click: false,
	    cache: [],
	    codigo: {
		linhas: [],
		posRam: [], //correspondencia entre variaveis no codigo e sua posicao na ram
		linhaAtual: 0,
	    	desenhaSeta: function (){
		    p.noStroke();
		    p.fill(255, 0, 0);
		    p.triangle(30, this.linhas[this.linhaAtual]-20,
			       30, this.linhas[this.linhaAtual]+20,
			       50, this.linhas[this.linhaAtual]);
		    p.rect(10, this.linhas[this.linhaAtual]-8, 30, 16);
		}},
	    ula: {}
	}

	//setup dos nucleos
	nucleo1 = Object.create(nucleo);
	nucleo1.codigo = Object.create(nucleo.codigo);
	nucleo1.codigo.linhas = [66,93,123,150];
	nucleo1.codigo.posRam = [false,0,1,false];
	nucleo1.cache = [Object.create(sprite).init("", 533, 82), Object.create(sprite).init("", 533, 127)];
	nucleo1.ula = Object.create(sprite).init("", 316, 105);
	nucleo1.ula.operacoes = ["N+1","Y+1"];
	nucleo1.ula.setTexto = setTextoUla;
	
	nucleo2 = Object.create(nucleo);
	nucleo2.codigo = Object.create(nucleo.codigo);
	nucleo2.codigo.linhas = [302,329,355,384];
	nucleo2.codigo.posRam = [false,1,2,false];
	nucleo2.cache = [Object.create(sprite).init("", 533, 316), Object.create(sprite).init("", 533, 369)];
	nucleo2.ula = Object.create(sprite).init("", 316, 335);
	nucleo2.ula.operacoes = ["Y+2","X+3"];
	nucleo2.ula.setTexto = setTextoUla;

	nucleos = [nucleo1, nucleo2];

	//setup da ram
	ram = [Object.create(sprite).init("N=1", 845, 170),
	       Object.create(sprite).init("Y=1", 845, 222),
	       Object.create(sprite).init("X=1", 845, 274)]
	ram.forEach(function (pos){
	    pos.marked = false;
	    pos.show = function (){
		p.fill(0);
		p.text(this.texto, this.x, this.y);

		if(this.marked){
		    p.fill(255,0,0);
		    p.rect(this.x, this.y, 100, 50);
		}
	    }
	});

	//o browser lembra o estado dos botões, então 
	//precisamos reiniciar tudo antes de começar o programa:
	reset();
    };

    p.draw = function (){
	p.image(img_cache_coherence, 0, 0);
	showAll();

	nucleos.forEach(function (nucleo){
	    var posRam;
	    var posCache;
	    
	    if(nucleo.click){
		linha = nucleo.codigo.linhaAtual;
		
		if(linha !== 0 && linha !== 3){
		    posRam = nucleo.codigo.posRam[linha];
		    posCache = (nucleo === nucleos[0]) ? posRam : posRam-1;
		    
		    if(wrtBack.checked){
			write([ram[posRam], nucleo.cache[posCache], nucleo.ula, nucleo.cache[posCache]]);
		    } else{
			write([ram[posRam], nucleo.cache[posCache], nucleo.ula, nucleo.cache[posCache], ram[posRam]]);
		    }

		    if(snoop.checked){
			
		    } else{
			
		    }
		} else{
		    reset();
		}
	    }
	});
    };

    sprite = {
	init: function (texto, x, y){
	    this.texto = texto;
	    this.x = x;
	    this.y = y;
	    this.dest = [];
	    return this;
	},
	show: function (){
	    p.fill(0);
	    p.text(this.texto, this.x, this.y);
	},
	setTexto: function (newTexto){
	    this.texto = newTexto;
	}
    };

    movimento = {
	init: function (path, velocidade){
	    this.velocidade = velocidade || 1;
	    this.path = path;
	    this.texto = path[0].texto;
	    this.x = path[0].x;
	    this.y = path[0].y;
	    this.finalX = path[1].x;
	    this.finalY = path[1].y;
	    this.anguloMovimento = p.atan2(this.finalY - this.y, this.finalX - this.x);
	    this.incX = p.cos(this.anguloMovimento) * velocidade;
	    this.incY = p.sin(this.anguloMovimento) * velocidade;
	    this.firstRun = true;
	    return this;
	},
	move: function (){
	    if(this.condParada()){
		this.afterMovement();
	    } else {
		this.x += this.incX;
		this.y += this.incY;
		p.text(this.texto, this.x, this.y);
	    }
	},
	condParada: function (){
	    return (p.round(this.x) === this.finalX) && (p.round(this.y) === this.finalY);
	},
	afterMovement: function (){
	    this.path[1].setTexto(this.texto);
	    if(this.path.length > 2){
		if(this.firstRun){
		    this.firstRun = false;
		    this.nextMove = Object.create(movimento).init(this.path.slice(1), this.velocidade);
		}
		this.nextMove.move();
	    } else{ //fim de uma sequencia de movimentos
		//		this.path[1].markedBy(this.path[0]);
		reset();
	    }
	}
    };

    write = function (sequencia){
	if(neverMoved){
	    writeMovement = Object.create(movimento).init(sequencia, 1);
	    neverMoved = false;
	}
	writeMovement.move();
    };
    
    clickBotao = function (nucleo){
	nucleo.codigo.linhaAtual = function (codigo){
	    if((codigo.linhaAtual + 1) >= codigo.linhas.length){
		return 0;
	    } else{
		return codigo.linhaAtual + 1;
	    }
	}(nucleo.codigo);
	nucleo.click = true;
    };

    showAll = function (){
	ram.forEach(function (bloco){
	    bloco.show();
	});
	nucleos.forEach(function (nucleo){
	    nucleo.codigo.desenhaSeta();
	    nucleo.cache.forEach(function (linhaCache){
		linhaCache.show();
	    });
	    nucleo.ula.show();
	});
    };

    reset = function (){
	btn1.disabled = false;
	btn2.disabled = false;
	neverMoved = true;
	wrtBack.disabled = false;
	wrtThrough.disabled = false;
	snoop.disabled = false;
	snarf.disabled = false;
	nucleos.forEach(function (nucleo){
	    nucleo.click = false;
	    nucleo.ula.texto = "";
	});
    };

    setTextoUla = function (newTexto){
	if(newTexto === ""){
	    this.texto = newTexto;
	} else{
	    //newTexto is supposed to be of the form: N=1
	    variavel = newTexto.split("=")[0];
	    operando1 = newTexto.split("=")[1];

	    //operacoes[posCache] is supposed to be of the form: Y+2
	    operador = this.operacoes[linha-1].charAt(1);
//	    operando2 = this.operacoes[linha-1].split(/[+*/-]/)[1];
	    operando2 = "1";
	    
	    this.texto = variavel + "=" + eval(operando1 + operador + operando2);
	}
    };

    btn1 = document.getElementById("btncore1");
    btn2 = document.getElementById("btncore2");

    wrtBack = document.getElementById("writeback")
    wrtBack.checked = true;
    wrtThrough = document.getElementById("writethrough");

    snoop = document.getElementById("snooping")
    snoop.checked = true;
    snarf = document.getElementById("snarfing");

    disableElements = function (){
    	btn1.disabled = true;
	btn2.disabled = true;
	wrtBack.disabled = true;
	wrtThrough.disabled = true;
	snoop.disabled = true;
	snarf.disabled = true;
    };

    btn1.onclick = function (){
	clickBotao(nucleos[0]);
	disableElements();
    };

    btn2.onclick = function (){
	clickBotao(nucleos[1])
	disableElements();
    };

    p.mouseClicked = function (){
	p.println(p.mouseX + "," + p.mouseY);
    };
}
